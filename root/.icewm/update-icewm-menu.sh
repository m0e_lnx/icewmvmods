#!/bin/bash
#
# Search .desktop files and extract information about installed programs
# Use the information to write menu entries for IceWM

# Check for X
if [ -z $DISPLAY ];then echo "X-windows required";exit;fi

# grab argument
choice="$1"
# Set file path 
filepath="$HOME/.icewm"

# Unless given "withicons" or "noicons" argument, present options.
if [ "$1" != "withicons" ] && [ "$1" != "noicons" ];then
choose=$(
export MAIN_DIALOG='
<window title="Update Menu">
 <vbox>
  <frame Search for new programs
and add them to the menu?>
  <hbox>
      <button>
        <label>Yes, refresh menu now</label>
        <action>echo search</action>
        <action type="exit">search</action>
      </button>
  </hbox>
  </frame>
  <frame  Reset menu to defaults? >
  <hbox>
      <button>
        <label>Yes, start from scratch</label>
        <action>echo search</action>
        <action type="exit">rebuild</action>
      </button>
  </hbox>
  </frame>
  <frame  Advanced >
    <checkbox active="true">
      <label>Use icons</label>
      <variable>ICONS</variable>
    </checkbox>
    <checkbox active="false">
      <label>Deep search</label>
      <variable>DEEPSEARCH</variable>
    </checkbox>
  </frame>
   <button cancel></button>
 </vbox>
</window>
'
gtkdialog --program=MAIN_DIALOG
)
else rebuild="yes"
fi

# cancel and exit?
if [ ! -z "$(echo $choose | grep "EXIT=\"Cancel\"" -o)" ];then exit;fi

# sort out what was chosen
if [ ! -z "$(echo $choose | grep "EXIT=\"search\"" -o)" ];then search="yes";fi
if [ ! -z "$(echo $choose | grep "EXIT=\"rebuild\"" -o)" ];then rebuild="yes";fi
if [ ! -z "$(echo $choose | grep "ICONS=\"false\"" -o)" ];then choice="noicons";fi
if [ ! -z "$(echo $choose | grep "ICONS=\"true\"" -o)" ];then choice="withicons";fi
if [ ! -z "$(echo $choose | grep "DEEPSEARCH=\"true\"" -o)" ];then deepsearch="yes";fi

# create backup dir if not there
if [ ! -e "$filepath/backups" ];then mkdir -p $filepath/backups;fi
# make backups of original files
cp $filepath/menu* $filepath/backups

if [ "$rebuild" == "yes" ];then
  mv $HOME/.icewm/menu $HOME/.icewm/backups
  mv $HOME/.icewm/menu-* $HOME/.icewm/backups
  if [ "$choice" == "withicons" ];then
    foldericon=""
    cp $HOME/.icewm/menu_withicons $HOME/.icewm/menu
    cp $HOME/.icewm/menu_system_withicons $HOME/.icewm/menu-system
#    cp $HOME/.icewm/menu_multimedia_withicons $HOME/.icewm/menu-multimedia
  fi
  if [ "$choice" == "noicons" ];then
    foldericon=noicon
    cp $HOME/.icewm/menu_noicons $HOME/.icewm/menu
    cp $HOME/.icewm/menu_system_noicons $HOME/.icewm/menu-system
#    cp $HOME/.icewm/menu_multimedia_noicons $HOME/.icewm/menu-multimedia
  fi
fi

searchcommand="ls /usr/share/applications/*.desktop"
if [ "$deepsearch" == "yes" ];then searchcommand="find /usr/share/applications -name "*.desktop"";fi

if [ "$search" == "yes" ] || [ "$rebuild" == "yes" ];then

# Start looking for new applications and their names.
echo "Searching for new applications"
$searchcommand | while read file;do

# Find executable.
if [ "$UID" == "0" ];then
  exe="$(cat "$file" | grep -i ^[Ee][Xx][Ee][Cc]= | sed s/[Ee][Xx][Ee][Cc]=// | sed s^\%[0-9a-zA-Z]^^ | sed s^gksu^^ | sed s/kdesu// | sed s^/sbin/vsuper^^ | sed 's/ *$//g' | sed "s/^ //g" | sed "s/  / /g" | sed 's/""//g' )"
  else
    exe="$(cat "$file" | grep -i ^[Ee][Xx][Ee][Cc]= | sed s/[Ee][Xx][Ee][Cc]=// | sed s^\%[0-9a-zA-Z]^^ | sed 's/ *$//' | sed s^kdesu^gksu^ | sed 's/ *$//g' | sed "s/^ //g" | sed "s/  / /g" | sed 's/""//g' )"
fi

if [  ! -z "$exe" ];then
 # Find name of application. If not there, substitute executable without arguments
  name="$(cat "$file" | grep -i ^[Nn][Aa][Mm][Ee]= | sed s/[Nn][Aa][Mm][Ee]=// | sed 's/[ \t]*$//')"
  if [ -z "$name" ];then name="$exe";fi

  # Find the category. If not in multimedia office graphics utilities network development system or games, it goes in other
    category=$(cat "$file" | grep -i categories | grep [=\;][Mm][Uu][Ll][Tt][Ii][Mm][Ee][Dd][Di][Aa] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Aa][Uu][Dd][Ii][Oo] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=audio;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Vv][Ii][Dd][Ee][Oo] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=video;fi
    fi
    if [ "$category" = "audio" ] || [ "$category" = "video" ];then
      category=multimedia
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Oo][Ff][Ff][Ii][Cc][Ee] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=office;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Dd][Ee][Vv][Ee][Ll][Oo][Pp][Mm][Ee][Nn][Tt] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=development;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Oo][Ff][Ff][Ii][Cc][Ee] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=office;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Gg][Rr][Aa][Pp][Hh][Ii][Cc][Ss] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=graphics;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Uu][Tt][Ii][Ll][Ii][Tt][Ii][Ee][Ss] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=utilities;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Uu][Tt][Ii][Ll][Ii][Tt][Yy] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=utility;fi
    fi
    if [ "$category" = "utility" ];then
      category=utilities
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Nn][Ee][Tt][Ww][Oo][Rr][Kk] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=network;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Ii][Nn][Tt][Ee][Rr][Nn][Ee][Tt] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=internet;fi
    fi
    if [ "$category" = "internet" ];then
      category=network
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Ss][Yy][Ss][Tt][Ee][Mm] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=system;fi
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Ss][Ee][Tt][Tt][Ii][Nn][Gg][Ss] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=settings;fi
    fi
    if [ "$category" = "settings" ];then
      category=system
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories | grep [=\;][Gg][Aa][Mm][Ee] -o | tr [:upper:] [:lower:] | sed 's/;//' | sed 's/=//')
      if [ ! -z "$category" ];then category=game;fi
    fi
    if [ "$category" = "game" ];then
      category=games
    fi
    if [ -z "$category" ];then
      category=$(cat "$file" | grep -i categories)
    fi
    if [ "$category" != "multimedia" ] && [ "$category" != "office" ] && [ "$category" != "graphics" ] && [ "$category" != "utilities" ] && [ "$category" != "network" ] && [ "$category" != "system" ] && [ "$category" != "games" ] && [ "$category" != "settings" ] && [ "$category" != "development" ];then 
     category=other
    fi

    #Since menu is captialized, make it so
    first=$(echo ${category:0:1} | tr 'a-z' 'A-Z')
    rest=$(echo ${category:1})
    Category="$first$rest"

    # file for menu category
    catfile="menu-$category"
    # icon for menu category
    if [ "$foldericon" != "noicon" ];then foldericon="/usr/share/icons/menu-$Category.png";fi

  # category file needs to exist and be referenced in menu file
  if [ ! -e "$filepath/$catfile" ];then touch $filepath/$catfile;fi

  checkcat=$(grep "menufile $Category" "$filepath/menu")
  if [ -z "$checkcat" ];then 
    echo "Adding menu category"
    menucat="menufile $Category $foldericon $catfile"
    sed -i "/prog dummy marker only/ a\ $menucat" "$filepath/menu"
  fi

  # Search for executable in menu file
  checkexe=1
  if [ -z "$(grep "\ $exe" -o "$filepath/$catfile")" ];then checkexe="0";fi
  if [ "$(grep "$exe" -o "$filepath/menu_exclude")" ];then checkexe="1";fi
  # Also search for the raw executable in the exclude file
  rawexe="$(cat "$file" | grep -i ^[Ee][Xx][Ee][Cc]= | sed s/[Ee][Xx][Ee][Cc]=// )"
  if [ "$(grep "$rawexe" -o "$filepath/menu_exclude")" ];then checkexe="1";fi

# set some values for no icons in menu
useicon="no"
finalicon="noicon"

# if exe is not already in menu, proceed and search for an icon
# Unless full icon path is specified, search for for a matching jpg, xpm or png file.
# If nothing found, use a default one.
if [  "$checkexe" == "0" ] && [ "$choice" == "withicons" ];then
    icon=$(cat "$file" | grep -i ^[Ii][Cc][Oo][Nn]= | sed 's/[Ii][Cc][Oo][Nn]=//')
    useicon=$icon
    if [ -z `echo $icon | grep \/` ];then
      useicon=
      for icondir in "/usr/share";do
    cd $icondir
    if [ -z $(echo "$icon" | grep png) ] && [ -z $(echo $icon | grep jpg) ] && [ -z $(echo $icon | grep xpm) ] && [ -z $(echo $icon | grep svg) ];then
      for icontype in jpg xpm png svg;do
        iconfound=$(find -name "$icon.$icontype" | sed 's/.\//\//')
        if [ -n "$iconfound" ];then
          useicon=$icondir$iconfound
        fi
      done
    else 
      iconfound=$(find -name "$icon" | sed 's/.\//\//')
      if [ -n "$iconfound" ];then
        useicon=$icondir$iconfound
      fi
    fi
      done
    fi
    if [ -z "$useicon" ];then
      useicon="/usr/share/icons/menu-Default.png"
    fi
    # If more than one icon found, use the first one listed.
    finalicon="$(echo $useicon|cut -d " " -f1)"

fi

    # if adding an item, give feedback on the menu update action
    if [  "$checkexe" == "0" ];then
    echo ==
    echo "Updating menu"
    echo "$file:"
    echo "Name: $name"
    echo "Executable: $exe"
    echo "Category: $Category"
    if [ -z "$useicon" ];then echo "No icon files found, using default";fi
    echo "Icon: $finalicon"

    # add the line in menu file
    echo "prog" "\"$name\"" "\"$finalicon\"" $exe >> $filepath/$catfile
    fi
fi

done
echo ""
echo "Finished"
sleep 1
fi