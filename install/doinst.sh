#!/bin/sh

# give some proper credit
if [ ! "$(grep Stefanussen usr/share/wallpapers/CREDITS)" ];then
  echo 'VL7-Light-*       : Dagfinn Stefanussen. Bardufoss, Norway' >> usr/share/wallpapers/CREDITS
fi

# obmixer expects alsamixergui to exist
if [ ! -e "usr/bin/alsamixergui" ];then ln -s aumix usr/bin/alsamixergui;fi

# address bug that causes the menu bookmark in pcmanfm not to work
if [ ! -e "etc/xdg/menus/applications.menu" ];then ln -s lxde-applications.menu etc/xdg/menus/applications.menu;fi

# write some config files
if [ ! -e "etc/skel/.config/pcmanfm/default" ];then 
mkdir -p etc/skel/.config/pcmanfm/default
cat > etc/skel/.config/pcmanfm/default/pcmanfm.conf <<EOF
[config]
bm_open_method=0
su_cmd=gksu %s

[volume]
mount_on_startup=1
mount_removable=1
autorun=1

[desktop]
wallpaper_mode=1
wallpaper=/usr/share/wallpapers/VL7-Light-BG-2.jpg
desktop_bg=#000000
desktop_fg=#ffffff
desktop_shadow=#000000
desktop_font=Sans 12
show_wm_menu=0

[ui]
always_show_tabs=0
max_tab_chars=32
win_width=640
win_height=480
splitter_pos=150
side_pane_mode=1
view_mode=0
show_hidden=0
sort_type=0
sort_by=2
EOF
fi

if [ ! -e "root/.config/pcmanfm/default" ];then 
mkdir -p root/.config/pcmanfm/default
cat > root/.config/pcmanfm/default/pcmanfm.conf <<EOF
[config]
bm_open_method=0
su_cmd=gksu %s

[volume]
mount_on_startup=1
mount_removable=1
autorun=1

[desktop]
wallpaper_mode=1
wallpaper=/usr/share/wallpapers/VL7-Light-BG-1.jpg
desktop_bg=#000000
desktop_fg=#ffffff
desktop_shadow=#000000
desktop_font=Sans 12
show_wm_menu=0

[ui]
always_show_tabs=0
max_tab_chars=32
win_width=640
win_height=480
splitter_pos=150
side_pane_mode=1
view_mode=0
show_hidden=1
sort_type=0
sort_by=2
EOF
fi

# copy files to user home dirs
for i in $(grep home /etc/passwd | cut -d":" -f1 | sed -e s/ftp//);do
  if [ ! -e "home/$i/.config/pcmanfm/default" ];then
    mkdir -p home/$i/.config/pcmanfm/default
    chown $i:$i home/$i/.config
    chown $i:$i home/$i/.config/pcmanfm
    chown $i:$i home/$i/.config/pcmanfm/default
  fi
  cp etc/skel/.config/pcmanfm/default/pcmanfm.conf home/$i/.config/pcmanfm/default
  chown $i:$i home/$i/.config/pcmanfm/default/pcmanfm.conf
  cp -r etc/skel/.icewm home/$i/
  chown -R $i:$i home/$i/.icewm
  if [ "$(grep gtk-icon-theme-name home/$i/.gtkrc-2.0)" ];then
	  echo "User account '$i' already has a defined gtk icon theme.  Not changing it"
    else
      echo ""  >> home/$i/.gtkrc-2.0
      echo "Setting default gtk icon theme for account '$i'"
      echo 'gtk-icon-theme-name="Faenza-Cupertino"' >> home/$i/.gtkrc-2.0
  fi
done

if [ "$(grep gtk-icon-theme-name root/.gtkrc-2.0)" ];then
  echo "root already has a GTK icon theme selected.  Not changing it"
  else
    echo "Setting default GTK Icon theme for 'root' account"
    echo "" >> root/.gtkrc-2.0
    echo 'gtk-icon-theme-name="Faenza-Cupertino"' >> root/.gtkrc-2.0
fi


