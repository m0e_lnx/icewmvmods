#!/bin/bash

SESSION_MANAGER=$(which lxsession-logout)
DIALOG=$(which zenity)
BANNER=/usr/share/pixmaps/VL1.png

# Test to make sure we have the session manager
if [ ! $SESSION_MANAGER ]; then
	msg="LXSession not found in this system.  Cannot exit X session this way."
	$DIALOG --error --text $msg
	exit 1
fi

exec /usr/bin/lxsession-logout --banner $BANNER --side top
